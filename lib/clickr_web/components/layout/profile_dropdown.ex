defmodule ClickrWeb.Components.Layout.ProfileDropdown do
  use ClickrWeb, :component

  prop entries, :list, required: true

  def render(assigns) do
    ~F"""
    <Context get={Layout, current_user: current_user}>
      <div x-data="{open: false}" @click.away="open = false" class="ml-3 relative">
        <div>
          <button x-on:click="open = !open" type="button" class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
            <span class="sr-only">Open user menu</span>
            <div class="flex items-center justify-center h-8 w-8 rounded-full bg-primary-500 font-bold text-lg">
              {String.first(current_user.email)}
            </div>
          </button>
        </div>

        <div x-show="open" x-cloak class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
          role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1"
          x-transition:enter="transition ease-out duration-100"
          x-transition:enter-start="transform opacity-0 scale-95"
          x-transition:enter-end="transform opacity-100 scale-100"
          x-transition:leave="transition ease-in duration-75"
          x-transition:leave-start="transform opacity-100 scale-100"
          x-transition:leave-end="transform opacity-0 scale-95"
        >
          <div class="block px-4 py-2 text-sm">
            {current_user.email}
          </div>

          {#for entry <- @entries}
            <.profile_entry {=entry} />
          {/for}
        </div>
      </div>
    </Context>
    """
  end

  # TODO Convert to surface functional components when undefined @__context__ error is solved
  defp profile_entry(%{entry: {:live_redirect, text, to}} = assigns) do
    ~H"""
    <%= live_redirect(text, to: to, class: profile_entry_class(), '@click': "open = false") %>
    """
  end

  defp profile_entry(%{entry: {:link, text, to, opts}} = assigns) do
    ~H"""
    <%= link(text, Keyword.merge(opts, to: to, class: profile_entry_class())) %>
    """
  end

  defp profile_entry_class(current \\ false),
    do: "#{current && "bg-gray-100"} block px-4 py-2 text-sm text-gray-700"
end
