defmodule ClickrWeb.SeatingPlanLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Rooms
  alias Clickr.Rooms.SeatingPlan

  data page_title, :string
  data seating_plans, :list
  data count, :integer
  data seating_plan, :map
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash} container={:xl}>
      <h1 class="text-center">Seating Plans</h1>

      {#if @live_action in [:new, :show]}
        <Components.ModalComponent
          component={ClickrWeb.SeatingPlanLive.FormComponent}
          id={@seating_plan.id || :new}
          title={@page_title}
          opts={seating_plan: @seating_plan, action: @live_action}
          return_to={Routes.seating_plans_path(@socket, :index)}
        />
      {/if}

      <div class="flex justify-center">
        <LivePatch label="New Seating Plan" to={Routes.seating_plan_path(@socket, :new)} class="mt-3 btn-primary" />
      </div>

      <SForm for={:search} change="search">
        <SForm.TextInput name={:query} opts={autofocus: true, placeholder: 'Search by class or room name', 'phx-debounce': 100} />
      </SForm>

      <Components.ContactCards class={"mt-5"}>
        {#for seating_plan <- @seating_plans}
          <LivePatch to={Routes.seating_plan_path(@socket, :show, seating_plan)}>
            <Components.Card>
              <Components.ContactCard name={"#{seating_plan.class.name} / #{seating_plan.room.name}"} />
            </Components.Card>
          </LivePatch>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        showing <b>{Enum.min([@limit, @count])}</b> of <b>{@count}</b> matching seating plans
      </div>
    </Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, list_seating_plans(socket)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :show, %{"id" => id}) do
    socket
    |> assign(:page_title, "Show Seating Plan")
    |> assign(:seating_plan, Rooms.get_seating_plan!(id, preload: [:class, :room]))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Seating Plan")
    |> assign(:seating_plan, %SeatingPlan{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Seating Plans")
    |> assign(:seating_plan, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    seating_plan = Rooms.get_seating_plan!(id)
    {:ok, _} = Rooms.delete_seating_plan(seating_plan)

    {:noreply, list_seating_plans(socket)}
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, list_seating_plans(socket, query: query)}
  end

  defp list_seating_plans(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {seating_plans, count} = Rooms.list_seating_plans(opts)
    assign(socket, seating_plans: seating_plans, count: count)
  end
end
