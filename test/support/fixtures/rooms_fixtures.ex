defmodule Clickr.RoomsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Clickr.Rooms` context.
  """

  @doc """
  Generate a room.
  """
  def room_fixture(attrs \\ %{}) do
    {:ok, room} =
      attrs
      |> Enum.into(%{
        buttons: [],
        name: "some name"
      })
      |> Clickr.Rooms.create_room()

    room
  end

  @doc """
  Generate a seating_plan.
  """
  def seating_plan_fixture(attrs \\ %{}) do
    class = Clickr.ClassesFixtures.class_fixture()
    room = room_fixture()

    {:ok, seating_plan} =
      attrs
      |> Enum.into(%{
        class_id: class.id,
        room_id: room.id,
        positioned_students: []
      })
      |> Clickr.Rooms.create_seating_plan()

    seating_plan
  end
end
