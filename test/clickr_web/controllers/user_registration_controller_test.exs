defmodule ClickrWeb.UserRegistrationControllerTest do
  use ClickrWeb.ConnCase, async: true

  import Clickr.AccountsFixtures

  describe "POST /users/register" do
    @tag :capture_log
    test "creates account and logs the user in", %{conn: conn} do
      email = unique_user_email()

      conn =
        post(conn, Routes.user_register_path(conn, :create), %{
          "user" => valid_user_attributes(email: email)
        })

      assert get_session(conn, :user_token)
      assert redirected_to(conn) == ClickrWeb.Helpers.UserAuth.signed_in_path(conn)

      # Now do a logged in request and assert on the menu
      conn = get(conn, "/")
      assert redirected_to(conn) == ClickrWeb.Helpers.UserAuth.signed_in_path(conn)
    end

    test "redirects with flash error message for invalid data", %{conn: conn} do
      conn =
        post(conn, Routes.user_register_path(conn, :create), %{
          "user" => %{"email" => "with spaces", "password" => "too short"}
        })

      assert redirected_to(conn) == Routes.user_register_path(conn, :new)
      assert get_flash(conn, :error) == "Failed to create user."
    end
  end
end
