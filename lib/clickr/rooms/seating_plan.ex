defmodule Clickr.Rooms.SeatingPlan do
  use Ecto.Schema
  import Ecto.Changeset

  alias Clickr.Classes.Class
  alias Clickr.Rooms.{PositionedStudent, Room}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "seating_plans" do
    belongs_to :class, Class
    belongs_to :room, Room
    embeds_many :positioned_students, PositionedStudent

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(seating_plan, attrs) do
    seating_plan
    |> cast(attrs, [:class_id, :room_id])
    |> cast_embed(:positioned_students)
    |> validate_required([])
    |> unique_constraint([:class, :room])
  end
end
