defmodule ClickrWeb.Components.Form.CheckboxRow do
  use ClickrWeb, :component

  slot label, required: false
  prop name, :atom, required: true
  prop class, :css_class, default: ["flex", "items-center"]

  def render(assigns) do
    ~F"""
    <SForm.Field name={@name} class={@class}>
      <SForm.Checkbox />
      <SForm.Label class="ml-2 block text-sm text-gray-900" />
    </SForm.Field>
    """
  end
end
