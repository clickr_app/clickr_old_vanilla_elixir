defmodule Clickr.Devices.Gateway do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "gateways" do
    field :type, Ecto.Enum, values: [:homeassistant]
    field :name, :string
    field :last_contact_at, :utc_datetime
    belongs_to :user, Clickr.Accounts.User

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(gateway, attrs) do
    gateway
    |> cast(attrs, [:type, :name, :last_contact_at, :user_id])
    |> validate_required([:type, :name, :user_id])
  end
end
