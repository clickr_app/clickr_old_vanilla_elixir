defmodule ClickrWeb.Components.ContactCards do
  use ClickrWeb, :component

  slot default, required: true
  prop class, :css_class, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <div role="list" class={"grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5" | @class}>
      <#slot />
    </div>
    """
  end
end
