defmodule Clickr.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false
      add :width, :integer, null: false
      add :height, :integer, null: false
      add :positioned_buttons, :jsonb

      timestamps(type: :utc_datetime)
    end

    create index(:rooms, [:name])
    create index(:classes, [:updated_at])
  end
end
