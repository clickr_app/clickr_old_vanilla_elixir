defmodule Clickr.DevicesTest do
  use Clickr.DataCase

  alias Clickr.Devices

  describe "devices" do
    alias Clickr.Devices.Device

    import Clickr.DevicesFixtures

    @invalid_attrs %{battery_level: nil, type: nil}

    test "list_devices/0 returns all devices" do
      device = device_fixture()
      assert Devices.list_devices() == [device]
    end

    test "get_device!/1 returns the device with given id" do
      device = device_fixture()
      assert Devices.get_device!(device.id) == device
    end

    test "create_device/1 with valid data creates a device" do
      gateway = gateway_fixture()
      valid_attrs = %{battery_level: 42, type: :ikea_tradfri_remote, gateway_id: gateway.id}

      assert {:ok, %Device{} = device} = Devices.create_device(valid_attrs)
      assert device.battery_level == 42
      assert device.type == :ikea_tradfri_remote
    end

    test "create_device/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Devices.create_device(@invalid_attrs)
    end

    test "update_device/2 with valid data updates the device" do
      device = device_fixture()
      update_attrs = %{battery_level: 43, type: :ikea_tradfri_remote}

      assert {:ok, %Device{} = device} = Devices.update_device(device, update_attrs)
      assert device.battery_level == 43
      assert device.type == :ikea_tradfri_remote
    end

    test "update_device/2 with invalid data returns error changeset" do
      device = device_fixture()
      assert {:error, %Ecto.Changeset{}} = Devices.update_device(device, @invalid_attrs)
      assert device == Devices.get_device!(device.id)
    end

    test "delete_device/1 deletes the device" do
      device = device_fixture()
      assert {:ok, %Device{}} = Devices.delete_device(device)
      assert_raise Ecto.NoResultsError, fn -> Devices.get_device!(device.id) end
    end

    test "change_device/1 returns a device changeset" do
      device = device_fixture()
      assert %Ecto.Changeset{} = Devices.change_device(device)
    end
  end

  describe "gateways" do
    alias Clickr.Devices.Gateway

    import Clickr.DevicesFixtures

    @invalid_attrs %{last_contact_at: nil, name: nil, type: nil}

    test "list_gateways/0 returns all gateways" do
      gateway = gateway_fixture()
      assert Devices.list_gateways() == [gateway]
    end

    test "get_gateway!/1 returns the gateway with given id" do
      gateway = gateway_fixture()
      assert Devices.get_gateway!(gateway.id) == gateway
    end

    test "create_gateway/1 with valid data creates a gateway" do
      user = Clickr.AccountsFixtures.confirmed_user_fixture()

      valid_attrs = %{
        last_contact_at: ~U[2021-09-30 13:20:00Z],
        name: "some name",
        type: :homeassistant,
        user_id: user.id
      }

      assert {:ok, %Gateway{} = gateway} = Devices.create_gateway(valid_attrs)
      assert gateway.last_contact_at == ~U[2021-09-30 13:20:00Z]
      assert gateway.name == "some name"
      assert gateway.type == :homeassistant
    end

    test "create_gateway/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Devices.create_gateway(@invalid_attrs)
    end

    test "update_gateway/2 with valid data updates the gateway" do
      gateway = gateway_fixture()

      update_attrs = %{
        last_contact_at: ~U[2021-10-01 13:20:00Z],
        name: "some updated name",
        type: :homeassistant
      }

      assert {:ok, %Gateway{} = gateway} = Devices.update_gateway(gateway, update_attrs)
      assert gateway.last_contact_at == ~U[2021-10-01 13:20:00Z]
      assert gateway.name == "some updated name"
      assert gateway.type == :homeassistant
    end

    test "update_gateway/2 with invalid data returns error changeset" do
      gateway = gateway_fixture()
      assert {:error, %Ecto.Changeset{}} = Devices.update_gateway(gateway, @invalid_attrs)
      assert gateway == Devices.get_gateway!(gateway.id)
    end

    test "delete_gateway/1 deletes the gateway" do
      gateway = gateway_fixture()
      assert {:ok, %Gateway{}} = Devices.delete_gateway(gateway)
      assert_raise Ecto.NoResultsError, fn -> Devices.get_gateway!(gateway.id) end
    end

    test "change_gateway/1 returns a gateway changeset" do
      gateway = gateway_fixture()
      assert %Ecto.Changeset{} = Devices.change_gateway(gateway)
    end
  end
end
