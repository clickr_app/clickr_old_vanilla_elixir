defmodule Clickr.Rooms.PositionedStudent do
  use Ecto.Schema
  import Ecto.Changeset

  alias Clickr.Students.Student

  embedded_schema do
    field :x, :integer
    field :y, :integer
    belongs_to :students, Student
  end

  def changeset(positioned_student, attrs) do
    positioned_student
    |> cast(attrs, [:x, :y, :student_id])
    |> validate_required([:x, :y, :student_id])
  end
end
