defmodule ClickrWeb.Helpers.UserLiveAuth do
  import Phoenix.LiveView
  alias Clickr.Accounts

  defmacro __using__(_opts) do
    quote do
      data current_user, :map
    end
  end

  def mount(_params, %{"user_token" => user_token}, socket) do
    socket = assign_new(socket, :current_user, fn -> fetch_current_user(user_token) end)
    user = socket.assigns.current_user

    if user do
      {:cont, socket}
    else
      {:halt,
       socket
       |> put_flash(:error, "You must log in to access this page.")
       |> redirect(ClickrWeb.Router.Helpers.user_log_in_path(socket, :new))}
    end
  end

  def fetch_current_user(user_token) do
    user_token && Accounts.get_user_by_session_token(user_token)
  end
end
