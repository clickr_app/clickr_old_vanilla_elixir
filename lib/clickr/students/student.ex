defmodule Clickr.Students.Student do
  use Ecto.Schema
  import Ecto.Changeset
  alias Clickr.Classes.Class

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "students" do
    field :name, :string
    belongs_to :class, Class

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(student, attrs) do
    student
    |> cast(attrs, [:name, :class_id])
    |> validate_required([:name])
  end
end
