defmodule ClickrWeb.StudentLive.FormComponent do
  use ClickrWeb, :live_component

  alias Clickr.Students

  prop title, :string, required: true
  prop action, :atom, required: true
  prop return_to, :string, required: true
  data changeset, :map
  data class, :map

  @impl true
  def render(assigns) do
    ~F"""
      <SForm for={@changeset} change="validate" submit="save" opts={id: "student-form"}>
        {#if @changeset.data.class_id}
          <Form.Row name={:class_id}>
            <SForm.HiddenInput />
            <div>{(if Ecto.assoc_loaded?(@changeset.data.class), do: @changeset.data.class, else: @class).name}</div>
          </Form.Row>
        {/if}

        <Form.Row name={:name}>
          <SForm.TextInput opts={required: true, autofocus: true} />
        </Form.Row>

        <div class="flex justify-center space-x-3">
          <Form.Submit label="Save" opts={'phx-disable-with': "Saving..."} />
          <button :if={@action == :show} :on-click="delete" class="btn-danger" phx-disable-with="Deleting...">Delete</button>
        </div>
      </SForm>
    """
  end

  @impl true
  def update(%{student: student} = assigns, socket) do
    changeset = Students.change_student(student)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"student" => student_params}, socket) do
    changeset =
      socket.assigns.student
      |> Students.change_student(student_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"student" => student_params}, socket) do
    save_student(socket, socket.assigns.action, student_params)
  end

  def handle_event("delete", _params, socket) do
    res = Students.delete_student(socket.assigns.student)

    case res do
      {:ok, _student} ->
        {:noreply,
         socket
         |> put_flash(:info, "Student deleted successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_student(socket, :show, student_params) do
    case Students.update_student(socket.assigns.student, student_params) do
      {:ok, _student} ->
        {:noreply,
         socket
         |> put_flash(:info, "Student updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_student(socket, :new, student_params) do
    case Students.create_student(student_params) do
      {:ok, _student} ->
        {:noreply,
         socket
         |> put_flash(:info, "Student created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
