defmodule ClickrWeb.Components.GridWithDragAndDrop do
  use ClickrWeb, :component

  prop id, :string, required: true
  prop data, :list, required: true
  prop class, :css_class, default: []
  prop opts, :keyword, default: []
  prop width, :integer, required: true
  prop height, :integer, required: true
  prop dragged, :event, required: true
  slot default, args: [item: ^data], required: true
  slot empty

  @impl true
  def render(assigns) do
    ~F"""
    <div
      x-data="{dragging: false}"
      role="list"
      class={"grid sm:gap-2 md:gap-3 lg:gap-4 xl:gap-6 auto-cols-fr" | @class}
      {...Map.new(@opts)}
      id={@id}
      :hook="GridWithDragAndDrop"
    >
      {#for %{x: x, y: y} = item <- @data}
        <div
          id={"#{@id}_source_#{x}_#{y}"}
          style={'grid-column': x, 'grid-row': y}
          draggable="true" class="flex flex-grow cursor-move"
          data={x: x, y: y}
          x-on:dragstart.self="
            dragging = true
            event.dataTransfer.effectAllowed = 'move'
            event.dataTransfer.setData('text/plain', event.target.id)
          "
          x-on:dragend.self="dragging = false"
        >
          <#slot :args={item: item} />
        </div>
      {/for}

      {#for {x, y} <- drop_targets(@data, @width, @height)}
        <div
          class="flex"
          style={'grid-column': x, 'grid-row': y}
          id={"#{@id}_target_#{x}_#{y}"}
          data={x: x, y: y}
          x-on:drop.self={"
            dragging = false
            const from = document.getElementById(event.dataTransfer.getData('text/plain')).dataset
            const to = event.target.dataset
            const hook = window.phxHooks.GridWithDragAndDrop[$root.id]
            #{if @dragged.target == :live_view do
              "hook.pushEvent('#{@dragged.name}', {from, to})"
            else
              "hook.pushEventTo('#{@dragged.target}', '#{@dragged.name}', {from, to})"
            end}
          "}
          x-on:dragover.self="dragging = $el"
          x-on:dragleave.self="dragging = true"
          :class="{
            'border-4 border-dashed border-gray-200': dragging,
            'border-gray-500': dragging == $el,
          }"
        >
          <div class="flex flex-grow" :class="{invisible: dragging}">
            <#slot name="empty" />
          </div>
        </div>
      {/for}
    </div>
    """
  end

  defp drop_targets(data, width, height) do
    used = MapSet.new(data, &{&1.x, &1.y})
    all = MapSet.new(for x <- 1..width, y <- 1..height, do: {x, y})
    MapSet.difference(all, used)
  end
end
