defmodule Clickr.Classes.Class do
  use Ecto.Schema
  import Ecto.Changeset
  alias Clickr.Students.Student
  alias Clickr.Rooms.{SeatingPlan, Room}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "classes" do
    field :name, :string
    has_many :students, Student
    many_to_many :rooms, Room, join_through: SeatingPlan

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(class, attrs) do
    class
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
