defmodule ClickrWeb.UserResetPasswordLive do
  defmodule New do
    use ClickrWeb, :live_view
    alias Clickr.Accounts

    @impl true
    def render(assigns) do
      ~F"""
      <Layout {=@flash}>
        <Components.CardWithHeading heading="Forgot your password?">
          <SForm for={:user} submit="submit">
            <Form.Row name={:email}>
              <SForm.EmailInput opts={required: true, autofocus: true} />
            </Form.Row>

            <div class="flex justify-center">
              <Form.Submit label="Send instructions to reset password" />
            </div>
          </SForm>
        </Components.CardWithHeading>
      </Layout>
      """
    end

    @impl true
    def handle_event("submit", %{"user" => %{"email" => email}}, socket) do
      if user = Accounts.get_user_by_email(email) do
        Accounts.deliver_user_reset_password_instructions(
          user,
          &Routes.user_reset_password_url(socket, :edit, &1)
        )
      end

      # In order to prevent user enumeration attacks, regardless of the outcome, show an impartial success/error message.
      {:noreply,
       put_flash(
         socket,
         :info,
         "If your email is in our system, you will receive instructions to reset your password shortly."
       )}
    end
  end

  defmodule Edit do
    use ClickrWeb, :live_view

    alias Clickr.Accounts

    data user, :map
    data token, :string
    data changeset, :map

    @impl true
    def render(assigns) do
      ~F"""
      <Layout {=@flash}>
        <Components.CardWithHeading heading="Reset password">
          <SForm for={@changeset} submit="submit">
            {#if @changeset.action && !@changeset.valid?}
              <p class="error">Oops, something went wrong! Please check the errors below.</p>
            {/if}

            <Form.Row name={:password} label_attrs={text: "New password"}>
              <SForm.PasswordInput value={Ecto.Changeset.get_field(@changeset, :password)} opts={required: true, autofocus: true} />
            </Form.Row>

            <Form.Row name={:password_confirmation} label_attrs={text: "Confirm new password"}>
              <SForm.PasswordInput value={Ecto.Changeset.get_field(@changeset, :password_confirmation)} opts={required: true} />
            </Form.Row>

            <div class="flex justify-center">
              <Form.Submit label="Reset password" />
            </div>
          </SForm>
        </Components.CardWithHeading>
      </Layout>
      """
    end

    @impl true
    def mount(%{"token" => token}, _session, socket) do
      if user = Accounts.get_user_by_reset_password_token(token) do
        changeset = Accounts.change_user_password(user)
        {:ok, assign(socket, user: user, token: token, changeset: changeset)}
      else
        {:ok,
         socket
         |> put_flash(:error, "Reset password link is invalid or it has expired.")
         |> redirect(to: "/")}
      end
    end

    @impl true
    def handle_event("submit", %{"user" => params}, socket) do
      case Accounts.reset_user_password(socket.assigns.user, params) do
        {:ok, _} ->
          {:noreply,
           socket
           |> put_flash(:info, "Password reset successfully.")
           |> redirect(to: Routes.user_log_in_path(socket, :new))}

        {:error, changeset} ->
          {:noreply, assign(socket, changeset: changeset)}
      end
    end
  end
end
