defmodule ClickrWeb.UserSessionLive do
  use ClickrWeb, :live_view

  alias Clickr.Accounts
  alias Clickr.Accounts.User

  data changeset, :map, default: Accounts.log_in_user(%User{})
  data error_message, :string, default: nil
  data trigger_action, :boolean, default: false

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@flash}>
      <Components.CardWithHeading>
        <:header>
          <h2 class="text-center">
            Log in to your account
          </h2>
          <p class="mt-2 text-center text-sm text-gray-600">
            Or
            <LiveRedirect label="register" to={Routes.user_register_path(@socket, :new)} class="font-medium" />
          </p>
        </:header>

        <SForm for={@changeset} action={Routes.user_log_in_path(@socket, :create)}
          submit="submit" opts={'phx-trigger-action': @trigger_action}>

          {#if @error_message}
            <p class="error">{@error_message}</p>
          {/if}

          <Form.Row name={:email}>
            <SForm.EmailInput opts={required: true, autofocus: true} />
          </Form.Row>

          <Form.Row name={:password}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@changeset, :password)} opts={required: true} />
          </Form.Row>

          <div class="flex items-center justify-between">
            <Form.CheckboxRow name={:remember_me} />
            <Link label="Forgot your password?" to={Routes.user_reset_password_path(@socket, :new)} class="text-sm font-medium" />
          </div>

          <div class="flex justify-center">
            <Form.Submit label="Log in" opts={'phx-disable-with': "Logging in..."} />
          </div>
        </SForm>
      </Components.CardWithHeading>
    </Layout>
    """
  end

  @impl true
  def handle_event("submit", %{"user" => params}, socket) do
    changeset = Accounts.log_in_user(%User{}, params)
    %{"email" => email, "password" => password} = params

    if Accounts.get_user_by_email_and_password(email, password) do
      {:noreply, assign(socket, changeset: changeset, trigger_action: true)}
    else
      # In order to prevent user enumeration attacks, don't disclose whether the email is registered.
      {:noreply, assign(socket, changeset: changeset, error_message: "Invalid email or password")}
    end
  end
end
