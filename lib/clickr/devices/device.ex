defmodule Clickr.Devices.Device do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "devices" do
    field :type, Ecto.Enum, values: [:ikea_tradfri_remote]
    field :battery_level, :integer
    field :last_contact_at, :utc_datetime
    belongs_to :gateway, Gateway

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(device, attrs) do
    device
    |> cast(attrs, [:type, :battery_level, :last_contact_at, :gateway_id])
    |> validate_required([:type, :gateway_id])
  end
end
