defmodule ClickrWeb.Components.Card do
  use ClickrWeb, :component

  slot default, required: true
  prop class, :css_class, default: []
  prop border?, :boolean, default: false

  @impl true
  def render(assigns) do
    ~F"""
    <div class={["bg-white py-8 px-4 sm:rounded-lg sm:px-10", shadow: !@border?, 'border border-black-300': @border?] ++  @class}>
      <#slot />
    </div>
    """
  end
end
