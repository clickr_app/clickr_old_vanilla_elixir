defmodule Clickr.RoomsTest do
  use Clickr.DataCase

  alias Clickr.Rooms

  describe "rooms" do
    alias Clickr.Rooms.Room

    import Clickr.RoomsFixtures

    @invalid_attrs %{positioned_buttons: nil, name: nil}

    test "list_rooms/0 returns all rooms" do
      room = room_fixture()
      assert Rooms.list_rooms() == {[room], 1}
    end

    test "get_room!/1 returns the room with given id" do
      room = room_fixture()
      assert Rooms.get_room!(room.id) == room
    end

    test "create_room/1 with valid data creates a room" do
      valid_attrs = %{positioned_buttons: [], name: "some name"}

      assert {:ok, %Room{} = room} = Rooms.create_room(valid_attrs)
      assert room.positioned_buttons == []
      assert room.name == "some name"
    end

    test "create_room/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rooms.create_room(@invalid_attrs)
    end

    test "update_room/2 with valid data updates the room" do
      room = room_fixture()
      update_attrs = %{positioned_buttons: [], name: "some updated name"}

      assert {:ok, %Room{} = room} = Rooms.update_room(room, update_attrs)
      assert room.positioned_buttons == []
      assert room.name == "some updated name"
    end

    test "update_room/2 with invalid data returns error changeset" do
      room = room_fixture()
      assert {:error, %Ecto.Changeset{}} = Rooms.update_room(room, @invalid_attrs)
      assert room == Rooms.get_room!(room.id)
    end

    test "delete_room/1 deletes the room" do
      room = room_fixture()
      assert {:ok, %Room{}} = Rooms.delete_room(room)
      assert_raise Ecto.NoResultsError, fn -> Rooms.get_room!(room.id) end
    end

    test "change_room/1 returns a room changeset" do
      room = room_fixture()
      assert %Ecto.Changeset{} = Rooms.change_room(room)
    end
  end

  describe "seating_plans" do
    alias Clickr.Rooms.SeatingPlan

    import Clickr.RoomsFixtures

    @invalid_attrs %{positioned_students: nil}

    test "list_seating_plans/0 returns all seating_plans" do
      seating_plan = seating_plan_fixture()
      assert Rooms.list_seating_plans() == {[seating_plan], 1}
    end

    test "get_seating_plan!/1 returns the seating_plan with given id" do
      seating_plan = seating_plan_fixture()
      assert Rooms.get_seating_plan!(seating_plan.id) == seating_plan
    end

    test "create_seating_plan/1 with valid data creates a seating_plan" do
      class = Clickr.ClassesFixtures.class_fixture()
      room = room_fixture()
      valid_attrs = %{positioned_students: [], class_id: class.id, room_id: room.id}

      assert {:ok, %SeatingPlan{} = seating_plan} = Rooms.create_seating_plan(valid_attrs)
      assert seating_plan.positioned_students == []
    end

    test "create_seating_plan/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rooms.create_seating_plan(@invalid_attrs)
    end

    test "update_seating_plan/2 with valid data updates the seating_plan" do
      seating_plan = seating_plan_fixture()
      update_attrs = %{positioned_students: []}

      assert {:ok, %SeatingPlan{} = seating_plan} =
               Rooms.update_seating_plan(seating_plan, update_attrs)

      assert seating_plan.positioned_students == []
    end

    test "update_seating_plan/2 with invalid data returns error changeset" do
      seating_plan = seating_plan_fixture()
      assert {:error, %Ecto.Changeset{}} = Rooms.update_seating_plan(seating_plan, @invalid_attrs)
      assert seating_plan == Rooms.get_seating_plan!(seating_plan.id)
    end

    test "delete_seating_plan/1 deletes the seating_plan" do
      seating_plan = seating_plan_fixture()
      assert {:ok, %SeatingPlan{}} = Rooms.delete_seating_plan(seating_plan)
      assert_raise Ecto.NoResultsError, fn -> Rooms.get_seating_plan!(seating_plan.id) end
    end

    test "change_seating_plan/1 returns a seating_plan changeset" do
      seating_plan = seating_plan_fixture()
      assert %Ecto.Changeset{} = Rooms.change_seating_plan(seating_plan)
    end
  end
end
