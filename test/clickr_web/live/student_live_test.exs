defmodule ClickrWeb.StudentLiveTest do
  use ClickrWeb.ConnCase

  import Phoenix.LiveViewTest
  import Clickr.StudentsFixtures

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  setup :register_and_log_in_user

  defp create_student(_) do
    student = student_fixture()
    %{student: student}
  end

  describe "Index" do
    setup [:create_student]

    test "lists all students", %{conn: conn, student: student} do
      {:ok, _index_live, html} = live(conn, Routes.students_path(conn, :index))

      assert html =~ "Students"
      assert html =~ student.name
    end

    test "saves new student", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, Routes.students_path(conn, :index))

      assert index_live |> element("a", "New Student") |> render_click() =~
               "New Student"

      assert_patch(index_live, Routes.student_path(conn, :new))

      assert index_live
             |> form("#student-form", student: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#student-form", student: @create_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.students_path(conn, :index))

      assert html =~ "Student created successfully"
      assert html =~ "some name"
    end

    test "updates student in listing", %{conn: conn, student: student} do
      {:ok, index_live, _html} = live(conn, Routes.students_path(conn, :index))

      assert index_live |> element("[x-test=student-#{student.id}-link]") |> render_click() =~
               "Show Student"

      assert_patch(index_live, Routes.student_path(conn, :show, student))

      assert index_live
             |> form("#student-form", student: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      {:ok, _, html} =
        index_live
        |> form("#student-form", student: @update_attrs)
        |> render_submit()
        |> follow_redirect(conn, Routes.students_path(conn, :index))

      assert html =~ "Student updated successfully"
      assert html =~ "some updated name"
    end

    # test "deletes student in listing", %{conn: conn, student: student} do
    #   {:ok, index_live, _html} = live(conn, Routes.student_path(conn, :show, student))

    #   assert index_live |> element("button", "Delete") |> render_click() |> follow_redirect(conn, Routes.students_path(conn, :index))
    #   refute has_element?(index_live, "#student-#{student.id}")
    # end
  end
end
