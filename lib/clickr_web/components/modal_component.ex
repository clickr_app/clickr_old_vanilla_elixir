defmodule ClickrWeb.Components.ModalComponent do
  use ClickrWeb, :live_component

  prop title, :string, required: true
  prop return_to, :string, required: true
  prop component, :atom, required: true
  prop opts, :keyword, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <div
      class="fixed z-50 left-0 top-0 w-screen h-screen overflow-auto bg-black bg-opacity-40 flex flex-col justify-center"
      :on-capture-click="close"
      :on-window-keydown="close"
      phx-key="escape"
      phx-page-loading>

      <Components.Card class="max-w-full !container">
        <LivePatch to={@return_to} class="text-gray-600 float-right text-3xl font-bold hover:text-black focus:text-black cursor-pointer">
          {Heroicons.Outline.x(class: 'w-6 h-6')}
        </LivePatch>
        <h2 class="text-center">{@title}</h2>
        {live_component(@socket, @component, Keyword.merge(@opts, id: @id, return_to: @return_to))}
      </Components.Card>
    </div>
    """
  end

  @impl true
  def handle_event("close", _, socket) do
    {:noreply, push_patch(socket, to: socket.assigns.return_to)}
  end
end
