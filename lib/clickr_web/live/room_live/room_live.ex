defmodule ClickrWeb.RoomLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Rooms
  alias Clickr.Rooms.Room

  data page_title, :string
  data rooms, :list
  data count, :integer
  data room, :map
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash} container={:xl}>
      <h1 class="text-center">Rooms</h1>

      {#if @live_action in [:new, :show]}
        <Components.ModalComponent
          component={ClickrWeb.RoomLive.FormComponent}
          id={@room.id || :new}
          title={@page_title}
          opts={room: @room, action: @live_action}
          return_to={Routes.rooms_path(@socket, :index)}
        />
      {/if}

      <div class="flex justify-center">
        <LivePatch label="New Room" to={Routes.room_path(@socket, :new)} class="mt-3 btn-primary" />
      </div>

      <SForm for={:search} change="search">
        <SForm.TextInput name={:query} opts={autofocus: true, placeholder: 'Search by room name', 'phx-debounce': 100} />
      </SForm>

      <Components.ContactCards class={"mt-5"}>
        {#for room <- @rooms}
          <LivePatch to={Routes.room_path(@socket, :show, room)}>
            <Components.Card>
              <Components.ContactCard name={room.name} />
            </Components.Card>
          </LivePatch>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        showing <b>{Enum.min([@limit, @count])}</b> of <b>{@count}</b> matching rooms
      </div>
    </Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, list_rooms(socket)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :show, %{"id" => id}) do
    socket
    |> assign(:page_title, "Show Room")
    |> assign(:room, Rooms.get_room!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Room")
    |> assign(:room, %Room{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Rooms")
    |> assign(:room, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    room = Rooms.get_room!(id)
    {:ok, _} = Rooms.delete_room(room)

    {:noreply, list_rooms(socket)}
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, list_rooms(socket, query: query)}
  end

  defp list_rooms(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {rooms, count} = Rooms.list_rooms(opts)
    assign(socket, rooms: rooms, count: count)
  end
end
