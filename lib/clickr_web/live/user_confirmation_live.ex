defmodule ClickrWeb.UserConfirmationLive do
  defmodule New do
    use ClickrWeb, :live_view
    alias Clickr.Accounts

    @impl true
    def render(assigns) do
      ~F"""
      <Layout {=@flash}>
        <Components.CardWithHeading heading="Resend confirmation instructions">
          <SForm for={:user} submit="submit">
            <Form.Row name={:email}>
              <SForm.EmailInput opts={required: true, autofocus: true} />
            </Form.Row>

            <div class="flex justify-center">
              <Form.Submit label="Resend confirmation instructions" />
            </div>
          </SForm>
        </Components.CardWithHeading>
      </Layout>
      """
    end

    @impl true
    def handle_event("submit", %{"user" => %{"email" => email}}, socket) do
      if user = Accounts.get_user_by_email(email) do
        Accounts.deliver_user_confirmation_instructions(
          user,
          &Routes.user_confirm_url(socket, :edit, &1)
        )
      end

      # In order to prevent user enumeration attacks, regardless of the outcome, show an impartial success/error message.
      {:noreply,
       put_flash(
         socket,
         :info,
         "If your email is in our system and it has not been confirmed yet, you will receive an email with instructions shortly."
       )}
    end
  end

  defmodule Edit do
    use ClickrWeb, :live_view

    alias Clickr.Accounts
    alias Clickr.Accounts.User

    data token, :string

    @impl true
    def render(assigns) do
      ~F"""
      <Layout {=@flash}>
        <Components.CardWithHeading heading="Confirm account">
          <SForm for={:user} submit="submit">
            <div class="flex justify-center">
              <Form.Submit label="Confirm my account" />
            </div>
          </SForm>
        </Components.CardWithHeading>
      </Layout>
      """
    end

    @impl true
    def mount(%{"token" => token}, _session, socket) do
      {:ok, assign(socket, token: token)}
    end

    # Do not log in the user after confirmation to avoid a
    # leaked token giving the user access to the account.
    @impl true
    def handle_event("submit", _params, socket) do
      case Accounts.confirm_user(socket.assigns.token) do
        {:ok, _} ->
          {:noreply,
           socket
           |> put_flash(:info, "User confirmed successfully.")
           |> redirect(to: ClickrWeb.Helpers.UserAuth.signed_in_path(socket))}

        :error ->
          # If there is a current user and the account was already confirmed,
          # then odds are that the confirmation link was already visited, either
          # by some automation or by the user themselves, so we redirect without
          # a warning message.
          cond do
            User.confirmed?(socket.assigns.current_user) ->
              {:noreply, redirect(socket, to: ClickrWeb.Helpers.UserAuth.signed_in_path(socket))}

            true ->
              {:noreply,
               socket
               |> put_flash(:error, "User confirmation link is invalid or it has expired.")
               |> redirect(to: "/")}
          end
      end
    end
  end
end
