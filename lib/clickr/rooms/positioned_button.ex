defmodule Clickr.Rooms.PositionedButton do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :x, :integer
    field :y, :integer
    field :button_id, :string
    field :button_type, :string
    belongs_to :device, Clickr.Devices.Device
  end

  def changeset(positioned_button, attrs) do
    positioned_button
    |> cast(attrs, [:x, :y, :button_id, :button_type, :device_id])
    |> validate_required([:x, :y, :button_id, :button_type, :device_id])
  end
end
