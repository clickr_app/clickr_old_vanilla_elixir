defmodule ClickrWeb.ClassLive.FormComponent do
  use ClickrWeb, :live_component

  alias Clickr.Classes
  alias Clickr.Students

  prop class, :map, required: true
  prop title, :string, required: true
  prop action, :atom, required: true
  prop return_to, :string, required: true
  data changeset, :map
  data students, :list
  data students_count, :list
  data students_limit, :integer, default: 9999

  @impl true
  def render(assigns) do
    ~F"""
    <div>
      <SForm for={@changeset} change="validate" submit="save" opts={id: "class-form"}>
        <Form.Row name={:name}>
          <SForm.TextInput opts={required: true, autofocus: true} />
        </Form.Row>

        <div class="flex justify-center space-x-3">
          <Form.Submit label="Save" opts={'phx-disable-with': "Saving..."} />
          <button :if={@action == :show} :on-click="delete" class="btn-danger" phx-disable-with="Deleting...">Delete</button>
        </div>
      </SForm>

      {#if @action == :show}
        <hr class="my-10"/>
        <div>
          <h3 class="text-center">Students</h3>

          <div class="flex justify-center">
            <LivePatch label="New Student" to={Routes.class_path(@socket, :new_student, @class.id)} class="mt-3 btn btn-secondary" />
          </div>

          <SForm for={:search_student} change="search_students">
            <SForm.TextInput name={:query} opts={placeholder: 'Search by student name', 'phx-debounce': 100} />
          </SForm>

          <Components.ContactCards class="mt-5">
            {#for student <- @students}
              <LivePatch opts={id: "student-#{student.id}"} to={Routes.class_path(@socket, :show_student, @class, student)}>
                <Components.Card border?={true}>
                  <Components.ContactCard name={student.name} />
                </Components.Card>
              </LivePatch>
            {/for}
          </Components.ContactCards>

          <div class="mt-5 text-center text-gray-700">
            showing <b>{Enum.min([@students_limit, @students_count])}</b> of <b>{@students_count}</b> matching students
          </div>
        </div>
      {/if}
    </div>
    """
  end

  @impl true
  def update(%{class: class} = assigns, socket) do
    changeset = Classes.change_class(class)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)
     |> list_students(class: class)}
  end

  @impl true
  def handle_event("validate", %{"class" => class_params}, socket) do
    changeset =
      socket.assigns.class
      |> Classes.change_class(class_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"class" => class_params}, socket) do
    save_class(socket, socket.assigns.action, class_params)
  end

  def handle_event("delete", _params, socket) do
    res = Classes.delete_class(socket.assigns.class)

    case res do
      {:ok, _class} ->
        {:noreply,
         socket
         |> put_flash(:info, "Class deleted successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  def handle_event("search_students", %{"query" => query}, socket) do
    {:noreply, list_students(socket, class: socket.assigns.class, query: query)}
  end

  defp save_class(socket, :show, class_params) do
    case Classes.update_class(socket.assigns.class, class_params) do
      {:ok, _class} ->
        {:noreply,
         socket
         |> put_flash(:info, "Class updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_class(socket, :new, class_params) do
    case Classes.create_class(class_params) do
      {:ok, _class} ->
        {:noreply,
         socket
         |> put_flash(:info, "Class created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp list_students(socket, opts) do
    {students, count} = Students.list_students(Keyword.merge([limit: 9999], opts))
    assign(socket, students: students, students_count: count)
  end
end
