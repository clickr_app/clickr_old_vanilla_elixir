defmodule Clickr.Students do
  @moduledoc """
  The Students context.
  """

  import Ecto.Query, warn: false
  alias Clickr.Repo

  alias Clickr.Students.Student

  @spec list_students(keyword) :: {[Student.t()], integer}
  @doc """
  Returns the list and count of students.

  ## Examples

      iex> list_students()
      {[%Student{}, ...], 3}

  """
  def list_students(opts \\ []) do
    query =
      Student
      |> where([s], ilike(s.name, ^"#{Keyword.get(opts, :query, "")}%"))
      |> maybe_where_class(Keyword.get(opts, :class))

    students =
      Repo.all(
        query
        |> limit(^Keyword.get(opts, :limit, 40))
        |> offset(^Keyword.get(opts, :offset, 0))
        |> order_by(^Keyword.get(opts, :order_by, desc: :updated_at))
        |> preload(^Keyword.get(opts, :preload, []))
      )

    count =
      Repo.one(
        query
        |> select([s], count(s.id))
      )

    {students, count}
  end

  defp maybe_where_class(query, %Clickr.Classes.Class{id: id}) when not is_nil(id),
    do: query |> where([s], s.class_id == ^id)

  defp maybe_where_class(query, _class), do: query

  @doc """
  Gets a single student.

  Raises `Ecto.NoResultsError` if the Student does not exist.

  ## Examples

      iex> get_student!(123)
      %Student{}

      iex> get_student!(456)
      ** (Ecto.NoResultsError)

  """
  def get_student!(id, opts \\ []) do
    Student
    |> where([s], s.id == ^id)
    |> preload(^Keyword.get(opts, :preload, []))
    |> Repo.one!()
  end

  @doc """
  Creates a student.

  ## Examples

      iex> create_student(%{field: value})
      {:ok, %Student{}}

      iex> create_student(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_student(attrs \\ %{}) do
    %Student{}
    |> Student.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a student.

  ## Examples

      iex> update_student(student, %{field: new_value})
      {:ok, %Student{}}

      iex> update_student(student, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_student(%Student{} = student, attrs) do
    student
    |> Student.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a student.

  ## Examples

      iex> delete_student(student)
      {:ok, %Student{}}

      iex> delete_student(student)
      {:error, %Ecto.Changeset{}}

  """
  def delete_student(%Student{} = student) do
    Repo.delete(student)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking student changes.

  ## Examples

      iex> change_student(student)
      %Ecto.Changeset{data: %Student{}}

  """
  def change_student(%Student{} = student, attrs \\ %{}) do
    Student.changeset(student, attrs)
  end
end
