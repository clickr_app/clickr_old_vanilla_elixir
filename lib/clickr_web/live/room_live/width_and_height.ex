defmodule ClickrWeb.RoomLive.WidthAndHeight do
  use ClickrWeb, :live_component
  alias ClickrWeb.RoomLive.FormComponent

  prop room, :map
  data min_width, :integer
  data min_height, :integer

  @impl true
  def render(assigns) do
    ~F"""
    <div class="flex flex-row justify-center gap-10">
      <div class="flex flex-row justify-center items-center gap-2">
        Width:
        <button :on-click="adjust_width" :values={delta: -1} class="btn-secondary w-0" disabled={@room.width <= @min_width}>-</button>
        {@room.width}
        <button :on-click="adjust_width" :values={delta: +1} class="btn-secondary w-0">+</button>
      </div>

      <div class="flex flex-row justify-center items-center gap-2">
        Height:
        <button :on-click="adjust_height" :values={delta: -1} class="btn-secondary w-0" disabled={@room.height <= @min_height}>-</button>
        {@room.height}
        <button :on-click="adjust_height" :values={delta: +1} class="btn-secondary w-0">+</button>
      </div>
    </div>
    """
  end

  # Callbacks
  @impl true
  def update(%{room: room} = assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign_min_width_and_height(room)}
  end

  @impl true
  def handle_event("adjust_width", %{"delta" => delta}, socket) do
    %{min_width: min, room: room} = socket.assigns
    FormComponent.change_width(room.id, enforce_min(room.width, delta, min))
    {:noreply, socket}
  end

  def handle_event("adjust_height", %{"delta" => delta}, socket) do
    %{min_height: min, room: room} = socket.assigns
    FormComponent.change_height(room.id, enforce_min(room.height, delta, min))
    {:noreply, socket}
  end

  # Internal
  defp enforce_min(current, delta_string, min) do
    delta = String.to_integer(delta_string)
    Enum.max([min, current + delta])
  end

  defp assign_min_width_and_height(socket, room) do
    buttons = room.positioned_buttons || []

    socket
    |> assign(min_width: min_size(Enum.map(buttons, & &1.x)))
    |> assign(min_height: min_size(Enum.map(buttons, & &1.y)))
  end

  defp min_size([]), do: 1
  defp min_size(sizes), do: Enum.max(sizes)
end
