defmodule ClickrWeb.Router do
  use ClickrWeb, :router

  import ClickrWeb.Helpers.UserAuth,
    only: [
      fetch_current_user: 2,
      redirect_if_user_is_authenticated: 2,
      require_authenticated_user: 2
    ]

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ClickrWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # Normal routes
  scope "/", ClickrWeb do
    pipe_through :browser

    live_session :default do
      live "/users/confirm", UserConfirmationLive.New, :new, as: :user_confirm
      live "/users/confirm/:token", UserConfirmationLive.Edit, :edit, as: :user_confirm
    end

    delete "/users/log_out", UserSessionController, :delete, as: :user_log_out
  end

  # Other scopes may use custom stacks.
  # scope "/api", ClickrWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: ClickrWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", ClickrWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :unauthenticated do
      live "/", UserSessionLive, :new, as: :landing
      live "/users/log_in", UserSessionLive, :new, as: :user_log_in
      live "/users/register", UserRegistrationLive, :new, as: :user_register
      live "/users/reset_password", UserResetPasswordLive.New, :new, as: :user_reset_password

      live "/users/reset_password/:token", UserResetPasswordLive.Edit, :edit,
        as: :user_reset_password
    end

    post "/users/register", UserRegistrationController, :create, as: :user_register
    post "/users/log_in", UserSessionController, :create, as: :user_log_in
  end

  scope "/", ClickrWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :authenticated, on_mount: ClickrWeb.Helpers.UserLiveAuth do
      live "/users/api_token", UserApiTokenLive, :new, as: :user_api_token
      live "/users/settings", UserSettingsLive, :edit, as: :user_settings

      live "/students", StudentLive, :index, as: :students
      live "/students/new", StudentLive, :new, as: :student
      live "/students/:id", StudentLive, :show, as: :student

      live "/classes", ClassLive, :index, as: :classes
      live "/classes/new", ClassLive, :new, as: :class
      live "/classes/:id", ClassLive, :show, as: :class
      live "/classes/:id/students/new", ClassLive, :new_student, as: :class
      live "/classes/:id/students/:student_id", ClassLive, :show_student, as: :class

      live "/rooms", RoomLive, :index, as: :rooms
      live "/room/new", RoomLive, :new, as: :room
      live "/room/:id", RoomLive, :show, as: :room

      live "/seating-plans", SeatingPlanLive, :index, as: :seating_plans
      live "/seating-plan/new", SeatingPlanLive, :new, as: :seating_plan
      live "/seating-plan/:id", SeatingPlanLive, :show, as: :seating_plan
    end

    put "/users/settings", UserSettingsController, :update, as: :user_settings

    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email,
      as: :user_settings
  end
end
