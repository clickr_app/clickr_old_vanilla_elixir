defmodule ClickrWeb.UserSessionControllerTest do
  use ClickrWeb.ConnCase, async: true

  import Clickr.AccountsFixtures

  setup do
    %{user: confirmed_user_fixture()}
  end

  describe "POST /users/log_in" do
    test "logs the user in", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.user_log_in_path(conn, :create), %{
          "user" => %{"email" => user.email, "password" => valid_user_password()}
        })

      assert get_session(conn, :user_token)
      assert redirected_to(conn) == ClickrWeb.Helpers.UserAuth.signed_in_path(conn)

      # Now do a logged in request and assert on the menu
      conn = get(conn, ClickrWeb.Helpers.UserAuth.signed_in_path(conn))
      response = html_response(conn, 200)
      assert response =~ user.email
      assert response =~ "Settings</a>"
      assert response =~ "Log out</a>"
    end

    test "logs the user in with remember me", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.user_log_in_path(conn, :create), %{
          "user" => %{
            "email" => user.email,
            "password" => valid_user_password(),
            "remember_me" => "true"
          }
        })

      assert conn.resp_cookies["_clickr_web_user_remember_me"]
      assert redirected_to(conn) == ClickrWeb.Helpers.UserAuth.signed_in_path(conn)
    end

    test "logs the user in with return to", %{conn: conn, user: user} do
      conn =
        conn
        |> init_test_session(user_return_to: "/foo/bar")
        |> post(Routes.user_log_in_path(conn, :create), %{
          "user" => %{
            "email" => user.email,
            "password" => valid_user_password()
          }
        })

      assert redirected_to(conn) == "/foo/bar"
    end

    test "redirects with flash error message with invalid credentials", %{conn: conn, user: user} do
      conn =
        post(conn, Routes.user_log_in_path(conn, :create), %{
          "user" => %{"email" => user.email, "password" => "invalid_password"}
        })

      assert redirected_to(conn) == Routes.user_log_in_path(conn, :new)
      assert get_flash(conn, :error) == "Invalid email or password"
    end
  end

  describe "DELETE /users/log_out" do
    test "logs the user out", %{conn: conn, user: user} do
      conn = conn |> log_in_user(user) |> delete(Routes.user_log_out_path(conn, :delete))
      assert redirected_to(conn) == "/users/log_in"
      refute get_session(conn, :user_token)
      assert get_flash(conn, :info) =~ "Logged out successfully"
    end

    test "succeeds even if the user is not logged in", %{conn: conn} do
      conn = delete(conn, Routes.user_log_out_path(conn, :delete))
      assert redirected_to(conn) == "/users/log_in"
      refute get_session(conn, :user_token)
      assert get_flash(conn, :info) =~ "Logged out successfully"
    end
  end
end
