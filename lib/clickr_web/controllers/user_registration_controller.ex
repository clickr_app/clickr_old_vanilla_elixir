defmodule ClickrWeb.UserRegistrationController do
  use ClickrWeb, :controller

  alias Clickr.Accounts
  alias ClickrWeb.Helpers.UserAuth

  def create(conn, %{"user" => user_params}) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &Routes.user_confirm_url(conn, :edit, &1)
          )

        conn
        |> put_flash(:info, "User created successfully.")
        |> UserAuth.log_in_user(user)

      {:error, %Ecto.Changeset{}} ->
        conn
        |> put_flash(:error, "Failed to create user.")
        |> redirect(to: Routes.user_register_path(conn, :new))
    end
  end
end
