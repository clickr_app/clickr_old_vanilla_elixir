defmodule ClickrWeb.StudentLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Students
  alias Clickr.Students.Student

  data page_title, :string
  data students, :list
  data count, :integer
  data student, :map
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash} container={:xl}>
      <h1 class="text-center">Students</h1>

      {#if @live_action in [:new, :show]}
        <Components.ModalComponent
          component={ClickrWeb.StudentLive.FormComponent}
          id={@student.id || :new}
          title={@page_title}
          opts={student: @student, action: @live_action}
          return_to={Routes.students_path(@socket, :index)}
        />
      {/if}

      <div class="flex justify-center">
        <LivePatch label="New Student" to={Routes.student_path(@socket, :new)} class="mt-3 btn-primary" />
      </div>

      <SForm for={:search} change="search">
        <SForm.TextInput name={:query} opts={autofocus: true, placeholder: 'Search by student name', 'phx-debounce': 100} />
      </SForm>

      <Components.ContactCards class={"mt-5"}>
        {#for student <- @students}
          <LivePatch to={Routes.student_path(@socket, :show, student)} opts={'x-test': "student-#{student.id}-link"}>
            <Components.Card border?={true}>
              <Components.ContactCard name={student.name} tag={student.class && student.class.name} />
            </Components.Card>
          </LivePatch>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        showing <b>{Enum.min([@limit, @count])}</b> of <b>{@count}</b> matching students
      </div>
    </Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, list_students(socket)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :show, %{"id" => id}) do
    socket
    |> assign(:page_title, "Show Student")
    |> assign(:student, Students.get_student!(id, preload: [:class]))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Student")
    |> assign(:student, %Student{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Students")
    |> assign(:student, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    student = Students.get_student!(id)
    {:ok, _} = Students.delete_student(student)

    {:noreply, list_students(socket)}
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, list_students(socket, query: query)}
  end

  defp list_students(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit, preload: [:class]], opts)
    {students, count} = Students.list_students(opts)
    assign(socket, students: students, count: count)
  end
end
