defmodule ClickrWeb.Components.Form.Row do
  use ClickrWeb, :component

  slot default, required: true
  prop name, :atom, required: true
  prop class, :css_class
  prop label_attrs, :keyword, default: []
  prop error_attrs, :keyword, default: []
  prop errors?, :boolean, default: true

  def render(assigns) do
    ~F"""
    <SForm.Field name={@name} class={@class}>
      <SForm.Label class="block text-sm font-medium text-gray-700" {...Map.new(@label_attrs)} />
      <div class="mt-1">
        <#slot />
        {#if @errors?}
          <SForm.ErrorTag class="error" {...Map.new(@error_attrs)} />
        {/if}
      </div>
    </SForm.Field>
    """
  end
end
