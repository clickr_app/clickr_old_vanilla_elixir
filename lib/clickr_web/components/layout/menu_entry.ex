defmodule ClickrWeb.Components.Layout.MenuEntry do
  use ClickrWeb, :component

  prop to, :string, required: true
  prop text, :string, required: true
  prop current, :boolean, default: false
  prop icon, :fun, required: true

  def render(assigns) do
    ~F"""
    <LiveRedirect {=@to} class={
      "group flex items-center px-2 py-2 text-sm font-medium rounded-md",
      'bg-gray-900 text-white': @current,
      'text-gray-300 hover:bg-gray-700 hover:text-white': !@current
    }>
      {@icon.(class: Surface.css_class([
        "mr-3 flex-shrink-0 h-6 w-6",
        "text-gray-300": @current,
        "text-gray-400 group-hover:text-gray-300": !@current
      ]))}
      {@text}
    </LiveRedirect>
    """
  end
end
