defmodule ClickrWeb.UserSettingsLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Accounts

  data email_changeset, :map
  data password_changeset, :map
  data trigger_password_action, :boolean, default: false

  def mount(_params, _session, socket) do
    user = socket.assigns.current_user

    {:ok,
     assign(socket,
       email_changeset: Accounts.change_user_email(user),
       password_changeset: Accounts.change_user_password(user)
     )}
  end

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash}>
      <h1 class="text-center">
        User Settings
      </h1>

      <Components.Card class={"mt-8"}>
        <h2 class="text-center">
          Change Email
        </h2>

        <SForm for={@email_changeset} submit="change_email">
          {#if @email_changeset.action && !@email_changeset.valid?}
            <p class="error">Oops, something went wrong! Please check the errors below.</p>
          {/if}

          <Form.Row name={:email}>
            <SForm.EmailInput opts={required: true, autofocus: true} />
          </Form.Row>

          <Form.Row name={:current_password} label_attrs={opts: [for: :current_password_for_email]}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@email_changeset, :current_password)} opts={id: :current_password_for_email, required: true} />
          </Form.Row>

          <div class="flex justify-center">
            <Form.Submit label="Change email" opts={'phx-disable-with': "Changing email..."} />
          </div>
        </SForm>
      </Components.Card>

      <Components.Card class={"mt-8"}>
        <h2 class="text-center">
          Change Password
        </h2>

        <SForm for={@password_changeset} action={Routes.user_settings_path(@socket, :update)}
          submit="change_password" opts={'phx-trigger-action': @trigger_password_action}>

          {#if @password_changeset.action && !@password_changeset.valid?}
            <p class="error">Oops, something went wrong! Please check the errors below.</p>
          {/if}

          <SForm.HiddenInput name={:action} value="update_password" />

          <Form.Row name={:password} label_attrs={text: "New password"}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@password_changeset, :password)} opts={required: true} />
          </Form.Row>

          <Form.Row name={:password_confirmation} label_attrs={text: "Confirm new password"}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@password_changeset, :password_confirmation)} opts={required: true} />
          </Form.Row>

          <Form.Row name={:current_password} label_attrs={opts: [for: :current_password_for_password]}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@password_changeset, :current_password)} opts={id: :current_password_for_password, required: true} />
          </Form.Row>

          <div class="flex justify-center">
            <Form.Submit label="Change password" opts={'phx-disable-with': "Changing password..."} />
          </div>
        </SForm>
      </Components.Card>
    </Layout>
    """
  end

  @impl true
  def handle_event("change_email", %{"user" => user_params}, socket) do
    %{"current_password" => password} = user_params
    user = socket.assigns.current_user

    case Accounts.apply_user_email(user, password, user_params) do
      {:ok, applied_user} ->
        Accounts.deliver_update_email_instructions(
          applied_user,
          user.email,
          &Routes.user_settings_url(socket, :confirm_email, &1)
        )

        {:noreply,
         socket
         |> put_flash(
           :info,
           "A link to confirm your email change has been sent to the new address."
         )
         |> assign(email_changeset: Accounts.change_user_email(user))}

      {:error, changeset} ->
        {:noreply, assign(socket, email_changeset: changeset)}
    end
  end

  @impl true
  def handle_event("change_password", %{"user" => params}, socket) do
    user = socket.assigns.current_user
    %{"current_password" => password} = params

    changeset =
      Accounts.change_user_password(user, params)
      |> Accounts.User.validate_current_password(password)
      |> Map.put(:action, :update)

    if changeset.valid? do
      {:noreply, assign(socket, password_changeset: changeset, trigger_password_action: true)}
    else
      {:noreply, assign(socket, password_changeset: changeset)}
    end
  end
end
