defmodule ClickrWeb.UserRegistrationLive do
  use ClickrWeb, :live_view
  alias Clickr.Accounts
  alias Clickr.Accounts.User

  data changeset, :map, default: Accounts.change_user_registration(%User{})
  data trigger_action, :boolean, default: false

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@flash}>
      <Components.CardWithHeading>
        <:header>
          <h2 class="text-center">
            Register your account
          </h2>
          <p class="mt-2 text-center text-sm text-gray-600">
            Or
            <LiveRedirect label="log in" to={Routes.user_log_in_path(@socket, :new)} class="font-medium" />
          </p>
        </:header>

        <SForm for={@changeset} action={Routes.user_register_path(@socket, :create)}
          opts={'phx-trigger-action': @trigger_action} submit="submit">

          {#if @changeset.action && !@changeset.valid?}
            <p class="error">Oops, something went wrong! Please check the errors below.</p>
          {/if}

          <Form.Row name={:email}>
            <SForm.EmailInput opts={required: true, autofocus: true} />
          </Form.Row>

          <Form.Row name={:password}>
            <SForm.PasswordInput value={Ecto.Changeset.get_field(@changeset, :password)} opts={required: true} />
          </Form.Row>

          <div class="flex justify-center">
            <Form.Submit label="Register" opts={'phx-disable-with': "Registering..."} />
          </div>
        </SForm>
      </Components.CardWithHeading>
    </Layout>
    """
  end

  @impl true
  def handle_event("submit", %{"user" => params}, socket) do
    changeset =
      %User{}
      |> Accounts.change_user_registration(params)
      |> Map.put(:action, :insert)

    case changeset.valid? do
      true -> {:noreply, assign(socket, changeset: changeset, trigger_action: true)}
      false -> {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
