defmodule Clickr.Repo.Migrations.CreateStudents do
  use Ecto.Migration

  def change do
    create table(:students, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false
      add :class_id, references(:classes, on_delete: :nothing, type: :binary_id)

      timestamps(type: :utc_datetime)
    end

    create index(:students, [:name])
    create index(:students, [:class_id])
    create index(:students, [:updated_at])
  end
end
