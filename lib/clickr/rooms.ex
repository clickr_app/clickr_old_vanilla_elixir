defmodule Clickr.Rooms do
  @moduledoc """
  The Rooms context.
  """

  import Ecto.Query, warn: false
  alias Clickr.Repo

  alias Clickr.Rooms.{SeatingPlan, Room}
  alias Clickr.Classes.Class

  @doc """
  Returns the list of rooms.

  ## Examples

      iex> list_rooms()
      {[%Room{}, ...], 3}

  """
  def list_rooms(opts \\ []) do
    query =
      Room
      |> where([s], ilike(s.name, ^"#{Keyword.get(opts, :query, "")}%"))

    rooms =
      Repo.all(
        query
        |> limit(^Keyword.get(opts, :limit, 40))
        |> offset(^Keyword.get(opts, :offset, 0))
        |> order_by(^Keyword.get(opts, :order_by, desc: :updated_at))
        |> preload(^Keyword.get(opts, :preload, []))
      )

    count =
      Repo.one(
        query
        |> select([s], count(s.id))
      )

    {rooms, count}
  end

  @doc """
  Gets a single room.

  Raises `Ecto.NoResultsError` if the Room does not exist.

  ## Examples

      iex> get_room!(123)
      %Room{}

      iex> get_room!(456)
      ** (Ecto.NoResultsError)

  """
  def get_room!(id, _opts \\ []) do
    Room
    |> where([r], r.id == ^id)
    |> Repo.one!()
  end

  @doc """
  Creates a room.

  ## Examples

      iex> create_room(%{field: value})
      {:ok, %Room{}}

      iex> create_room(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_room(attrs \\ %{}) do
    %Room{}
    |> Room.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a room.

  ## Examples

      iex> update_room(room, %{field: new_value})
      {:ok, %Room{}}

      iex> update_room(room, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_room(%Room{} = room, attrs) do
    room
    |> Room.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a room.

  ## Examples

      iex> delete_room(room)
      {:ok, %Room{}}

      iex> delete_room(room)
      {:error, %Ecto.Changeset{}}

  """
  def delete_room(%Room{} = room) do
    Repo.delete(room)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking room changes.

  ## Examples

      iex> change_room(room)
      %Ecto.Changeset{data: %Room{}}

  """
  def change_room(%Room{} = room, attrs \\ %{}) do
    Room.changeset(room, attrs)
  end

  def move_positioned_button(%Room{} = room, %{x: from_x, y: from_y}, %{x: to_x, y: to_y}) do
    old_buttons = room.positioned_buttons
    old = Enum.find(old_buttons, &(&1.x == from_x && &1.y == from_y))

    if old do
      without_old = Enum.reject(old_buttons, &(&1 == old))
      with_new = [%{old | x: to_x, y: to_y} | without_old]
      update_room(room, %{positioned_buttons: with_new})
    else
      {:error, "Cannot find old position"}
    end
  end

  alias Clickr.Rooms.SeatingPlan

  @doc """
  Returns the list of seating_plans.

  ## Examples

      iex> list_seating_plans()
      {[%SeatingPlan{}, ...], 3}

  """
  def list_seating_plans(opts \\ []) do
    query =
      if Keyword.has_key?(opts, :query) do
        SeatingPlan
        |> join(:inner, [s], c in Class, on: s.class_id == c.id)
        |> join(:inner, [s], r in Room, on: s.room_id == r.id)
        |> where(
          [s, c, r],
          ilike(c.name, ^"#{Keyword.get(opts, :query, "")}%") or
            ilike(r.name, ^"#{Keyword.get(opts, :query, "")}%")
        )
      else
        SeatingPlan
      end

    seating_plans =
      Repo.all(
        query
        |> limit(^Keyword.get(opts, :limit, 40))
        |> offset(^Keyword.get(opts, :offset, 0))
        |> order_by(^Keyword.get(opts, :order_by, desc: :updated_at))
        |> preload(^Keyword.get(opts, :preload, []))
      )

    count =
      Repo.one(
        query
        |> select([s], count(s.id))
      )

    {seating_plans, count}
  end

  @doc """
  Gets a single seating_plan.

  Raises `Ecto.NoResultsError` if the Class in room does not exist.

  ## Examples

      iex> get_seating_plan!(123)
      %SeatingPlan{}

      iex> get_seating_plan!(456)
      ** (Ecto.NoResultsError)

  """
  def get_seating_plan!(id, opts \\ []) do
    SeatingPlan
    |> preload(^Keyword.get(opts, :preload, []))
    |> where([s], id: ^id)
    |> Repo.one!()
  end

  @doc """
  Creates a seating_plan.

  ## Examples

      iex> create_seating_plan(%{field: value})
      {:ok, %SeatingPlan{}}

      iex> create_seating_plan(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_seating_plan(attrs \\ %{}) do
    %SeatingPlan{}
    |> SeatingPlan.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a seating_plan.

  ## Examples

      iex> update_seating_plan(seating_plan, %{field: new_value})
      {:ok, %SeatingPlan{}}

      iex> update_seating_plan(seating_plan, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_seating_plan(%SeatingPlan{} = seating_plan, attrs) do
    seating_plan
    |> SeatingPlan.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a seating_plan.

  ## Examples

      iex> delete_seating_plan(seating_plan)
      {:ok, %SeatingPlan{}}

      iex> delete_seating_plan(seating_plan)
      {:error, %Ecto.Changeset{}}

  """
  def delete_seating_plan(%SeatingPlan{} = seating_plan) do
    Repo.delete(seating_plan)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking seating_plan changes.

  ## Examples

      iex> change_seating_plan(seating_plan)
      %Ecto.Changeset{data: %SeatingPlan{}}

  """
  def change_seating_plan(%SeatingPlan{} = seating_plan, attrs \\ %{}) do
    SeatingPlan.changeset(seating_plan, attrs)
  end
end
