defmodule ClickrWeb.Components.Form.Submit do
  use ClickrWeb, :component
  use Surface.Components.Events

  import Surface.Components.Utils, only: [events_to_opts: 1]

  prop label, :string
  prop class, :css_class
  prop opts, :keyword, default: []
  slot default

  def render(assigns) do
    props = prop_to_attr_opts(assigns.class, :class)
    events = events_to_opts(assigns)

    opts =
      assigns.opts
      |> Keyword.merge(props)
      |> Keyword.merge(events)

    assigns = assign(assigns, opts: opts)

    ~F"""
    <button type="submit" class="btn-primary" :attrs={@opts}>
      <#slot>{@label}</#slot>
    </button>
    """
  end
end
