defmodule ClickrWeb.UserSettingsController do
  use ClickrWeb, :controller

  alias Clickr.Accounts
  alias ClickrWeb.Helpers.UserAuth

  def update(conn, %{"action" => "update_password", "user" => params}) do
    %{"current_password" => password} = params
    user = conn.assigns.current_user

    case Accounts.update_user_password(user, password, params) do
      {:ok, user} ->
        conn
        |> put_session(:user_return_to, Routes.user_settings_path(conn, :edit))
        |> put_session(:user_flash, "Password updated successfully.")
        |> UserAuth.log_in_user(user)

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to update password")
        |> redirect(to: Routes.user_settings_path(conn, :edit))
    end
  end

  def confirm_email(conn, %{"token" => token}) do
    case Accounts.update_user_email(conn.assigns.current_user, token) do
      :ok ->
        conn
        |> put_flash(:info, "Email changed successfully.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))

      :error ->
        conn
        |> put_flash(:error, "Email change link is invalid or it has expired.")
        |> redirect(to: Routes.user_settings_path(conn, :edit))
    end
  end
end
