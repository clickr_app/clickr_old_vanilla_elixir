defmodule ClickrWeb.Components.Layout.Flash do
  use ClickrWeb, :component

  prop type, :atom, values!: [:info, :error]
  prop text, :string

  def render(assigns) do
    type = Map.get(assigns, :type, :info)

    colors =
      case type do
        :info ->
          %{
            bg: 'bg-green-50',
            icon: 'text-green-400',
            text: 'text-green-700',
            btn:
              'text-green-500 hover:bg-green-100 focus:ring-offset-green-50 focus:ring-green-600'
          }

        :error ->
          %{
            bg: 'bg-red-50',
            icon: 'text-red-400',
            text: 'text-red-700',
            btn: 'text-red-500 hover:bg-red-100 focus:ring-offset-red-50 focus:ring-red-600'
          }
      end

    ~F"""
    {#if @text}
      <div class="sm:mx-auto sm:w-full sm:max-w-md mb-4">
        <div class={"rounded-md p-4 #{colors.bg}"}>
          <div class="flex">
            <div class="flex-shrink-0">
              {Heroicons.Solid.check_circle(class: "h-5 w-5 #{colors.icon}")}
            </div>
            <div class="ml-3">
              <p class={"text-sm font-medium #{colors.text}"}>
                {@text}
              </p>
            </div>
            <div class="ml-auto pl-3">
              <div class="-mx-1.5 -my-1.5">
                <button :on-click="lv:clear-flash" :values={key: @type} type="button" class={"inline-flex rounded-md p-1.5 focus:outline-none focus:ring-2 focus:ring-offset-2 #{colors.btn}"}>
                  <span class="sr-only">Dismiss</span>
                  {Heroicons.Solid.x(class: "h-5 w-5")}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    {/if}
    """
  end
end
