defmodule Clickr.Repo.Migrations.CreateGateways do
  use Ecto.Migration

  def change do
    create table(:gateways, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :type, :string, null: false
      add :name, :string, null: false
      add :last_contact_at, :utc_datetime
      add :user_id, references(:users, on_delete: :nothing, type: :binary_id), null: false

      timestamps(type: :utc_datetime)
    end

    create index(:gateways, [:name])
    create index(:gateways, [:type])
    create index(:gateways, [:last_contact_at])
  end
end
