defmodule Clickr.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :type, :string, null: false
      add :battery_level, :integer
      add :last_contact_at, :utc_datetime
      add :gateway_id, references(:gateways, on_delete: :nothing, type: :binary_id), null: false

      timestamps(type: :utc_datetime)
    end

    create index(:devices, [:type])
    create index(:devices, [:last_contact_at])
    create index(:devices, [:gateway_id])
  end
end
