defmodule Clickr.DevicesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Clickr.Devices` context.
  """

  @doc """
  Generate a device.
  """
  def device_fixture(attrs \\ %{}) do
    {:ok, device} =
      attrs
      |> Enum.into(%{
        battery_level: 42,
        type: :ikea_tradfri_remote,
        gateway_id: gateway_fixture().id
      })
      |> Clickr.Devices.create_device()

    device
  end

  @doc """
  Generate a gateway.
  """
  def gateway_fixture(attrs \\ %{}) do
    {:ok, gateway} =
      attrs
      |> Enum.into(%{
        last_contact_at: ~U[2021-09-30 13:20:00Z],
        name: "some name",
        type: :homeassistant,
        user_id: Clickr.AccountsFixtures.confirmed_user_fixture().id
      })
      |> Clickr.Devices.create_gateway()

    gateway
  end
end
