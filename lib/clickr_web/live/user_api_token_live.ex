defmodule ClickrWeb.UserApiTokenLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Accounts

  data token, :string, default: nil

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash}>
      <Components.CardWithHeading heading="Create API Token">
        {#if @token}
          <div x-data="{}" x-on:click="$refs.token.select(); navigator.clipboard.writeText($refs.token.value)" class="flex gap-3 items-center text-gray-700 border-2 border-black-500 mb-3 p-2">
            <input class="font-mono text-xs flex-grow" value={@token} x-ref="token" />
            <button>{Heroicons.Outline.clipboard_copy(class: "h-6 w-6")}</button>
          </div>
        {/if}

        <div class="flex justify-center">
          <button :on-click="generate" class="btn-primary">Create</button>
        </div>
      </Components.CardWithHeading>
    </Layout>
    """
  end

  @impl true
  def handle_event("generate", _params, socket) do
    token = Accounts.create_api_token(socket.assigns.current_user)
    {:noreply, assign(socket, token: token)}
  end
end
