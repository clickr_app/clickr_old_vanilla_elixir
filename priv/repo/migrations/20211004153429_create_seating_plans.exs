defmodule Clickr.Repo.Migrations.CreateSeatingPlans do
  use Ecto.Migration

  def change do
    create table(:seating_plans, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :class_id, references(:classes, on_delete: :nothing, type: :binary_id), null: false
      add :room_id, references(:rooms, on_delete: :nothing, type: :binary_id), null: false
      add :positioned_students, :jsonb

      timestamps(type: :utc_datetime)
    end

    create unique_index(:seating_plans, [:class_id, :room_id])
    create index(:seating_plans, [:class_id])
    create index(:seating_plans, [:room_id])
    create index(:seating_plans, [:updated_at])
  end
end
