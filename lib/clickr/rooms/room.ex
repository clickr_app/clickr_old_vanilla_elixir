defmodule Clickr.Rooms.Room do
  use Ecto.Schema
  import Ecto.Changeset
  alias Clickr.Rooms.{PositionedButton, SeatingPlan}
  alias Clickr.Classes.Class

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "rooms" do
    field :name, :string
    field :width, :integer, default: 8
    field :height, :integer, default: 4
    embeds_many :positioned_buttons, PositionedButton
    many_to_many :classes, Class, join_through: SeatingPlan

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:name, :width, :height])
    |> cast_embed(:positioned_buttons)
    |> validate_required([:name, :width, :height])
  end
end
