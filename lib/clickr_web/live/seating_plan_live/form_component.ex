defmodule ClickrWeb.SeatingPlanLive.FormComponent do
  use ClickrWeb, :live_component

  # alias Clickr.Rooms
  # alias ClickrWeb.RoomLive.WidthAndHeight

  # prop title, :string, required: true
  # prop action, :atom, required: true
  # prop return_to, :string, required: true
  # data room, :map
  # data changeset, :map

  @impl true
  def render(assigns) do
    ~F"""
    <div>
      {!--<SForm for={@changeset} change="validate" submit="save" opts={id: "room-form"}>
        <Form.Row name={:name}>
          <SForm.TextInput opts={required: true, autofocus: true} />
        </Form.Row>

        <div class="flex justify-center space-x-3">
          <Form.Submit label="Save" opts={'phx-disable-with': "Saving..."} />
          <button :if={@action == :show} :on-click="delete" class="btn-danger" phx-disable-with="Deleting...">Delete</button>
        </div>
      </SForm>

      {#if @action == :show}
        <hr class="my-10"/>

        <div>
          <h3 class="text-center">Buttons</h3>
          <WidthAndHeight id="width_and_height" room={@room} />

          <Components.GridWithDragAndDrop id="room_grid" dragged="button_dragged" width={@room.width} height={@room.height} class="mt-5" data={[%{x: 1, y: 1, text: 'hey'}, %{x: 2, y: 2, text: 'ho'}]}>
            <:default :let={item: item}>
              <Components.Card border?={true} class={"truncate !p-0 flex flex-grow justify-center items-center"}>
                {item.text}
              </Components.Card>
            </:default>

            <:empty>
              <div class="flex flex-grow justify-center items-center">
                {Heroicons.Outline.plus_circle(class: "h-12 w-12 text-gray-500")}
              </div>
            </:empty>
          </Components.GridWithDragAndDrop>
        </div>
      {/if}--}
    </div>
    """
  end

  # Public API
  def change_width(room_id, width), do: send_update(__MODULE__, id: room_id, width: width)
  def change_height(room_id, height), do: send_update(__MODULE__, id: room_id, height: height)

  # Callbacks
  @impl true
  def update(%{room: room} = assigns, socket) do
    {:ok,
     socket
     |> assign(assigns)
     |> assign_room_and_changeset(room)}
  end

  def update(%{width: width}, socket) do
    {:ok, room} = Rooms.update_room(socket.assigns.room, %{width: width})
    {:ok, assign_room_and_changeset(socket, room)}
  end

  def update(%{height: height}, socket) do
    {:ok, room} = Rooms.update_room(socket.assigns.room, %{height: height})
    {:ok, assign_room_and_changeset(socket, room)}
  end

  def update(assigns, socket) do
    {:ok, assign(socket, assigns)}
  end

  @impl true
  def handle_event("validate", %{"room" => room_params}, socket) do
    changeset =
      socket.assigns.room
      |> Rooms.change_room(room_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"room" => room_params}, socket) do
    save_room(socket, socket.assigns.action, room_params)
  end

  def handle_event("delete", _params, socket) do
    res = Rooms.delete_room(socket.assigns.room)

    case res do
      {:ok, _room} ->
        {:noreply,
         socket
         |> put_flash(:info, "Room deleted successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  def handle_event("button_dragged", %{"from" => from, "to" => to}, socket) do
    from = parse_coordinates(from)
    to = parse_coordinates(to)
    {:ok, room} = Rooms.move_positioned_button(socket.assigns.room, from, to)

    {:noreply, assign(socket, room: room)}
  end

  defp parse_coordinates(%{"x" => x, "y" => y}),
    do: %{x: String.to_integer(x), y: String.to_integer(y)}

  defp save_room(socket, :show, room_params) do
    case Rooms.update_room(socket.assigns.room, room_params) do
      {:ok, _room} ->
        {:noreply,
         socket
         |> put_flash(:info, "Room updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_room(socket, :new, room_params) do
    case Rooms.create_room(room_params) do
      {:ok, _room} ->
        {:noreply,
         socket
         |> put_flash(:info, "Room created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp assign_room_and_changeset(socket, room) do
    changeset = Rooms.change_room(room)
    assign(socket, room: room, changeset: changeset)
  end
end
