defmodule Clickr.Classes do
  @moduledoc """
  The Classes context.
  """

  import Ecto.Query, warn: false
  alias Clickr.Repo

  alias Clickr.Classes.Class

  @spec list_classes(keyword) :: {[Class.t()], integer}
  @doc """
  Returns the list of classes.

  ## Examples

      iex> list_classes()
      {[%Class{}, ...], 3}

  """
  def list_classes(opts \\ []) do
    query =
      Class
      |> where([s], ilike(s.name, ^"#{Keyword.get(opts, :query, "")}%"))

    classes =
      Repo.all(
        query
        |> limit(^Keyword.get(opts, :limit, 40))
        |> offset(^Keyword.get(opts, :offset, 0))
        |> order_by(^Keyword.get(opts, :order_by, desc: :updated_at))
        |> preload(^Keyword.get(opts, :preload, []))
      )

    count =
      Repo.one(
        query
        |> select([s], count(s.id))
      )

    {classes, count}
  end

  @doc """
  Gets a single class.

  Raises `Ecto.NoResultsError` if the Class does not exist.

  ## Examples

      iex> get_class!(123)
      %Class{}

      iex> get_class!(456)
      ** (Ecto.NoResultsError)

  """
  def get_class!(id, opts \\ []) do
    Class
    |> where([c], c.id == ^id)
    |> preload(^Keyword.get(opts, :preload, []))
    |> Repo.one!()
  end

  @doc """
  Creates a class.

  ## Examples

      iex> create_class(%{field: value})
      {:ok, %Class{}}

      iex> create_class(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_class(attrs \\ %{}) do
    %Class{}
    |> Class.changeset(attrs)
    |> Repo.insert()
  end

  def create_student(class, attrs \\ %{}) do
    Ecto.build_assoc(class, :students, attrs)
  end

  @doc """
  Updates a class.

  ## Examples

      iex> update_class(class, %{field: new_value})
      {:ok, %Class{}}

      iex> update_class(class, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_class(%Class{} = class, attrs) do
    class
    |> Class.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a class.

  ## Examples

      iex> delete_class(class)
      {:ok, %Class{}}

      iex> delete_class(class)
      {:error, %Ecto.Changeset{}}

  """
  def delete_class(%Class{} = class) do
    Repo.delete(class)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking class changes.

  ## Examples

      iex> change_class(class)
      %Ecto.Changeset{data: %Class{}}

  """
  def change_class(%Class{} = class, attrs \\ %{}) do
    Class.changeset(class, attrs)
  end
end
