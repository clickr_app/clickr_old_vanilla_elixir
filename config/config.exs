# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :clickr,
  ecto_repos: [Clickr.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :clickr, ClickrWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "0aeoVl5A6hp/YHpZeUfaHnizLzsKo9McEzPnYX9whuqIgVRNvSXOoTK8h6qcz428",
  render_errors: [view: ClickrWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Clickr.PubSub,
  live_view: [signing_salt: "vVulHUZu"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :clickr, Clickr.Mailer, adapter: Swoosh.Adapters.Local

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.12.18",
  default: [
    args: ~w(js/app.js --bundle --target=es2016 --outdir=../priv/static/assets),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

alias Surface.Components.Form

input_class =
  "appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm"

config :surface, :components, [
  {Form, default_class: "space-y-6"},
  {Form.Checkbox,
   default_class: "h-4 w-4 text-primary-600 focus:ring-primary-500 border-gray-300 rounded"},
  {Form.ErrorTag, default_translator: {ClickrWeb.ErrorHelpers, :translate_error}},
  {Form.EmailInput, default_class: input_class},
  {Form.NumberInput, default_class: input_class},
  {Form.PasswordInput, default_class: input_class},
  {Form.TextInput, default_class: input_class}
]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
