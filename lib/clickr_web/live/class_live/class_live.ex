defmodule ClickrWeb.ClassLive do
  use ClickrWeb, :live_view
  use UserLiveAuth

  alias Clickr.Classes
  alias Clickr.Classes.Class
  alias Clickr.Students

  data page_title, :string
  data classes, :list
  data count, :integer
  data class, :map
  data student, :map
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Layout {=@current_user} {=@flash} container={:xl}>
      <h1 class="text-center">Classes</h1>

      {#if @live_action in [:new, :show]}
        <Components.ModalComponent
          component={ClickrWeb.ClassLive.FormComponent}
          id={@class.id || :new}
          title={@page_title}
          opts={class: @class, action: @live_action}
          return_to={Routes.classes_path(@socket, :index)}
        />
      {/if}

      {#if @live_action in [:new_student, :show_student]}
        <Components.ModalComponent
          component={ClickrWeb.StudentLive.FormComponent}
          id={@student.id || :new}
          title={@page_title}
          opts={student: @student, class: @class, action: %{new_student: :new, show_student: :show}[@live_action]}
          return_to={Routes.class_path(@socket, :show, @class.id)}
        />
      {/if}

      <div class="flex justify-center">
        <LivePatch label="New Class" to={Routes.class_path(@socket, :new)} class="mt-3 btn-primary" />
      </div>

      <SForm for={:search} change="search">
        <SForm.TextInput name={:query} opts={autofocus: true, placeholder: 'Search by class name', 'phx-debounce': 100} />
      </SForm>

      <Components.ContactCards class={"mt-5"}>
        {#for class <- @classes}
          <LivePatch to={Routes.class_path(@socket, :show, class)}>
            <Components.Card>
              <Components.ContactCard name={class.name} />
            </Components.Card>
          </LivePatch>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        showing <b>{Enum.min([@limit, @count])}</b> of <b>{@count}</b> matching classes
      </div>
    </Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, list_classes(socket)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :show, %{"id" => id}) do
    socket
    |> assign(:page_title, "Show Class")
    |> get_class(id)
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Class")
    |> assign(:class, %Class{})
  end

  defp apply_action(socket, :new_student, %{"id" => id}) do
    socket_with_class =
      socket
      |> assign(:page_title, "New Student")
      |> get_class(id)

    socket_with_class
    |> assign(:student, Classes.create_student(socket_with_class.assigns.class))
  end

  defp apply_action(socket, :show_student, %{"id" => id, "student_id" => student_id}) do
    socket
    |> assign(:page_title, "Show Student")
    |> get_class(id)
    |> assign(:student, Students.get_student!(student_id))
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Classes")
    |> assign(:class, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    class = Classes.get_class!(id)
    {:ok, _} = Classes.delete_class(class)

    {:noreply, list_classes(socket)}
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, list_classes(socket, query: query)}
  end

  defp list_classes(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {classes, count} = Classes.list_classes(opts)
    assign(socket, classes: classes, count: count)
  end

  def get_class(socket, id) do
    assign(socket, :class, Classes.get_class!(id))
  end
end
